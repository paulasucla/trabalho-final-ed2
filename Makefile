############################# Makefile ##########################

all: indexer

indexer: indexer.c
	gcc -o indexer indexer.c -lm -W -Wall -std=c99 -pedantic

clean:
	rm -rf *~ indexer
