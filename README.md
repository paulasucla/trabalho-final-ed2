# Trabalho final ED2

## Descrição

O programa **indexer** faz a leitura de um arquivos de texto. A partir dai, o programa possibilita através de comandos especificos, que o usuário possa conferir o número de ocorrência das palavras que aparencem com mais frequência em um arquivo. Além disso, realiza a contagem de uma palavra especifica. E por fim, pode apresentar uma listagem dos documentos mais relevantes para um dado termo de busca, utilizando o cálculo TF-IDF.

## Instruções

Clone o projeto em sua máquina

Através do terminal de comando utilize o seguinte comando para criar o programa

```
gcc -o indexer indexer.c -lm -W -Wall -std=c99 -pedantic
```

## Opções de comando
```
--freq N ARQUIVO
```
Exibe o número de ocorrência das N palavras que mais aparecem em ARQUIVO, em ordem decrescente de ocorrência.
```  
--freq-word PALAVRA ARQUIVO
```
Exibe o número de ocorrências de PALAVRA em ARQUIVO. 
```
--search TERMO ARQUIVO [ARQUIVO ...]
```
Exibe uma listagem dos ARQUIVOS mais relevantes encontrados pela busca por TERMO. A listagem é apresentada em ordem descrescente de relevância. TERMO pode conter mais de uma palavra. Neste caso, deve ser indicado entre àspas.

## Observações 

Os termos pesquisados no programa **devem** estar em letra MINÚSCULA

Caracteres que não são letras serão IGNORADOS




