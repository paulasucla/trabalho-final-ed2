#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <getopt.h>
#include <math.h>

#define MAX_CHAR 26
#define CHAR_TO_INDEX(c) ((int)c - (int)'a')

struct PalavraFreq
{
	char *palavra;
	int freq;
};

struct TermoTFIDF
{
	char *nome_arquivo;
	float tfidf;
};

//nó da Trie
struct Node
{
	int flag; // indica o final da palavra
	int frequencia; // numero de ocorrencias de uma palavra
	int indiceMinHeap; // indice de uma palavra na Heap
	struct Node* filho[MAX_CHAR]; // 26 espaços de 'a' a 'z'.
};

// nó da MinHeap
struct MinHeapNode
{
	struct Node* raiz; // indica o nó folha da trie
	int frequencia;
	char* palavra;
};

// Min Heap
struct MinHeap
{
	int capacidade; // tamanho da min heap (N)
	int count; // qtde dos espaços preenchidos
	struct MinHeapNode* array; // vetor de minHeapNodes
};

//difinição dos parametros que o programa aceita
struct option Opcoes[] = {
        {"freq", required_argument, 0, 'f'},
        {"freq-word", required_argument, NULL, 'w'},
        {"search", required_argument, NULL, 's'},
        {0, 0, 0, 0}
};

struct Node* root = NULL;

double total_palavras;

//separa a string em palavras pelo delimitador (todo caractere que nao faz parte do alfabeto)
char *strtok_t(char *str, int (*test)(int ch)){
    static char *store = NULL;
    char *token;
    if(str != NULL){
        store = str;
    }
    if(store == NULL) return NULL;
    while(*store && !test(*store)){
        ++store;
    }
    if(*store == '\0') return NULL;
    token=store;
    while(*store && test(*store)){
        ++store;
    }

    if(*store == '\0'){
        store = NULL;
    } else {
        *store++ = '\0';
    }
    return token;
}

// funcão que cria um novo nó da Trie
struct Node* newTrieNode()
{
	// Aloca memória para o novo nó
	struct Node* trieNode = NULL;
	int i;
	trieNode = (struct Node *)malloc(sizeof(struct Node));
	trieNode->flag = 0;
	trieNode->frequencia = 0;
	trieNode->indiceMinHeap = -1;
	for( i = 0; i < MAX_CHAR; ++i )
		trieNode->filho[i] = NULL;

	return trieNode;
}

// funcão que cria a MinHeap de acordo com a capacidade recebida (N)
struct MinHeap* createMinHeap( int capacidade )
{
	struct MinHeap* minHeap = NULL; //nova MinHeap;
	minHeap = (struct MinHeap *)malloc(sizeof(struct MinHeap));
	if (minHeap == NULL){
		printf("ERRO");
		exit(0);
	}
	minHeap->capacidade = capacidade;
	minHeap->count = 0;

	// Aloca memoria para o vetor de nós min heap
	struct MinHeapNode* minHeapNode = (struct MinHeapNode *)malloc((minHeap->capacidade * sizeof(int) * sizeof(struct MinHeapNode)));
	minHeap->array = minHeapNode;
	if (minHeap->array == NULL){
		printf("Erro de memória");
	}
	return minHeap;
}

// funcção para trocar dois nós min heap
void swapMinHeapNodes ( struct MinHeapNode* a, struct MinHeapNode* b )
{
	struct MinHeapNode temp = *a;
	*a = *b;
	*b = temp;
}

//Reorganiza a minHeap para manter a propriedade heap
void minHeapify( struct MinHeap* minHeap, int index )
{
	int esq, dir, menor;

	esq = 2 * index + 1;
	dir = 2 * index + 2;
	menor = index;
	if ( esq < minHeap->count &&
		minHeap->array[ esq ]. frequencia <
		minHeap->array[ menor ]. frequencia
	)
		menor = esq;

	if ( dir < minHeap->count &&
		minHeap->array[ dir ]. frequencia <
		minHeap->array[ menor ]. frequencia
	)
		menor = dir;

	if( menor != index )
	{
		minHeap->array[ menor ].raiz->indiceMinHeap = index;
		minHeap->array[ index ].raiz->indiceMinHeap = menor;

		swapMinHeapNodes (&minHeap->array[ menor ], &minHeap->array[ index ]);
		minHeapify( minHeap, menor );
	}
}

// Função para montar uma heap
void buildMinHeap( struct MinHeap* minHeap )
{
	int n, i;
	n = minHeap->count - 1;

	for( i = ( n - 1 ) / 2; i >= 0; --i )
		minHeapify( minHeap, i );
}

// Insere uma palavra na heap
void insereMinHeap( struct MinHeap* minHeap, struct Node** root, const char* str )
{
	// se a palavra se está presenta na minHeap
	if( (*root)->indiceMinHeap != -1 )
	{
		++( minHeap->array[ (*root)->indiceMinHeap ]. frequencia );
		minHeapify( minHeap, (*root)->indiceMinHeap );
	}
	// se a palavra não está presente e a heap não está cheia
	else if( minHeap->count < minHeap->capacidade )
	{
		int count = minHeap->count;
		minHeap->array[ count ]. frequencia = (*root)->frequencia;
		minHeap->array[ count ].palavra= (char *)malloc(sizeof(strlen(str) + 1));
		strcpy( minHeap->array[ count ].palavra, str );
		minHeap->array[ count ].raiz= *root;
		(*root)->indiceMinHeap = minHeap->count;
		++( minHeap->count );
		buildMinHeap( minHeap );
	}
	// se a palavra não está presente e a heap está cheia.
	else if ( (*root)->frequencia > minHeap->array[0]. frequencia )
	{
		minHeap->array[ 0 ].raiz->indiceMinHeap = -1;
		minHeap->array[ 0 ].raiz = *root;
		minHeap->array[ 0 ].raiz->indiceMinHeap = 0;
		minHeap->array[ 0 ]. frequencia = (*root)->frequencia;

		free (minHeap->array[ 0 ].palavra);
		minHeap->array[ 0 ].palavra = (char *)malloc(sizeof(strlen(str) + 1));
		strcpy( minHeap->array[ 0 ].palavra, str );

		minHeapify ( minHeap, 0 );
	}
}

// Insere uma nova palavra na Trie e Heap
void insereTrieHeap ( struct Node** root, struct MinHeap* minHeap,
						const char* word, const char* Word )
{
	// se a arvore estiver vazia
	if ( *root == NULL )
		*root = newTrieNode();

	// se ainda existem mais caracteres em word
	if ( *word != '\0' )
		insereTrieHeap ( &((*root)->filho[ tolower( *word ) - 97 ]),minHeap, word + 1, Word );
	else // a palavra e processada completamente
	{
		// se ja esta presente entao aumenta a frequencia
		if ( (*root)->flag )
			++( (*root)->frequencia );
		else
		{
			(*root)->flag = 1;
			(*root)->frequencia = 1;
		}
		total_palavras+=1;
		// insere na heap
		insereMinHeap( minHeap, root, Word );
	}
}

// função que ordena e mostra o conteúdo da minHeap (N palavras mais frequentes)
void showMinHeap( struct MinHeap* minHeap )
{
	int i,j,N;
	N=minHeap->count;
	struct PalavraFreq array[N],key;

	for( i = 0; i<N; ++i )
	{
		array[i].freq=minHeap->array[i].frequencia;
		array[i].palavra=minHeap->array[i].palavra;
	}
	for (i = 1; i < N; i++) {
          key = array[i];
          j = i - 1;
          while (j >= 0 && array[j].freq > key.freq) {
            array[j + 1] = array[j];
            j = j - 1;
          }
          array[j + 1] = key;
        }
	printf("Palavra\t\tOcorrencias");
	for(i=N-1;i>=0;i--)
		printf("\n%-16s%d",array[i].palavra,array[i].freq);
	printf("\n");
}

// le o arquivo de entrada, separa em palavras, adiciona na Trie e Heap
struct MinHeap* topNPalavras( FILE* arq, int N )
{
	char str[100],*word;
	// Cria a Min Heap de tamanho N
	struct MinHeap* minHeap = createMinHeap( N );

	while (!feof(arq)){
		// Lê uma linha (inclusive com o '\n')
		// o 'fgets' lê até 99 caracteres ou até o '\n'
		fgets(str, 100, arq);
		if (str!=NULL){  // Se foi possível ler
			for (word = strtok_t(str, isalpha); word; word=strtok_t(NULL, isalpha))
			insereTrieHeap(&root, minHeap, word, word);
		}
	}
	return minHeap;
}

int search(struct Node *root, const char *key)
{
    int level;
    int length = strlen(key);
    int index;
    struct Node *pCrawl = root;


    for (level = 0; level < length; level++)
    {
        index = CHAR_TO_INDEX(key[level]);
	if (index < 0)
        	printf("\nindex: %d %c\n",index,key[level]);
        if (!pCrawl->filho[index])
            return 0;

        pCrawl = pCrawl->filho[index];

    }

    if (pCrawl != NULL && pCrawl->flag)
      return pCrawl->frequencia;
    else
      return 0;
}

float term_freq(FILE *d, char *termo,float *z ){
	float tf;
	int freq;
	total_palavras=0;
	topNPalavras (d, 3);
	freq=search(root, termo);
	if (freq!=0){
		*z+=1;
		tf=freq/total_palavras;
		free(root);
		fclose(d);
		root=NULL;
	}
	return tf;
}

void mostra_tfidf(int D,char *arq[],float idf,float *tf){
	struct TermoTFIDF* termoTFIDF = NULL;
	struct TermoTFIDF aux ;
	int i,j;
	termoTFIDF = (struct TermoTFIDF *)malloc(sizeof(struct TermoTFIDF) * (sizeof(D)));
	for (i=0;i<D;i++){
		termoTFIDF[i].nome_arquivo=arq[i+3];
		termoTFIDF[i].tfidf=tf[i]*idf;
	}
	for (i = 1; i < D; i++) {
		aux = termoTFIDF[i];
		j = i - 1;
		while (j >= 0 && termoTFIDF[j].tfidf > aux.tfidf) {
			termoTFIDF[j + 1] = termoTFIDF[j];
			j = j - 1;
		}
		termoTFIDF[j + 1] = aux;
	}
	printf("ARQUIVO\t\tTFIDF\n\n");
	for (i=D-1;i>=0;i--)
		printf("%-16s%.3f\n",termoTFIDF[i].nome_arquivo,termoTFIDF[i].tfidf);
}

int main(int argc, char *argv[]) {
	FILE *arq;
	char optc = 0;
	int N,i,D,freq;
	float *vet_tf,idf,z=0;

	if(argc == 1) { // Sem argumentos
                printf("Parametros faltando\n");
                exit(0);
        }
	//interpreta os parametros e armazena a opcao em optc
	optc = getopt_long(argc, argv, ":fwsa:", Opcoes, NULL);
	D=argc-3; //qtde de arquivos
	switch(optc){
		case 'f':
			arq = fopen(argv[3], "r");
			N = atoi(optarg);
			if (arq == NULL)
				printf ("Arquivo nao encontrado");
			else
				showMinHeap( topNPalavras (arq, N) );
			break;
		case 'w':
			arq = fopen(argv[3], "r");
			topNPalavras (arq, 3);
			freq=search(root, optarg);
			if (freq!=0)
				printf("Palavra\t\tOcorrencias\n%s\t\t%d\n",optarg,freq);
			else
				printf("\nPalavra NAO encontrada!\n");
			break;
		case 's':
			vet_tf = (float *)malloc(sizeof(D));
			for (i=0;i<D;i++){ //le os arquivos passados por parametro e calcula a TF de acordo com cada um
				arq = fopen(argv[i+3], "r");
				vet_tf[i]=term_freq(arq,optarg,&z);
			}
			idf=log10(D/z);
			//calcula o tfidf do termo para um arquivo
			mostra_tfidf(D,argv,idf,vet_tf);
			break;
		default :
			printf("Parametro incorreto\n");
			exit(0);
	}
	return 0;
}

